### N-1 plots
# BEGIN PLOT /MC_VBSVAL/Nm1_lep1_pt
Title=1st lepton $p_T$ (N-1 plot)
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_lep2_pt
Title=2nd lepton $p_T$ (N-1 plot)
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_lep1_eta
Title=1st lepton $\eta$ (N-1 plot)
XLabel=$\eta$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_lep2_eta
Title=2nd lepton $\eta$ (N-1 plot)
XLabel=$\eta$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_mll
Title=Lepton invariant mass (N-1 plot)
XLabel=m(ll) (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_mll_Zveto
Title=Lepton invariant mass (N-1 plot, Z veto)
XLabel=m(ll) (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_jet1_pt
Title=1st jet $p_T$ (N-1 plot)
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_jet2_pt
Title=2nd jet $p_T$ (N-1 plot)
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_jet1_eta
Title=1st jet $\eta$ (N-1 plot)
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_jet2_eta
Title=2nd jet $\eta$ (N-1 plot)
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_mjj
Title=di-jet invariant mass (N-1 plot)
XLabel=m(jj) (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_NJets
Title=Jet Multiplicity (N-1 plot)
XLabel=Number of Jets
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_missingET
Title=Missing Momentum (N-1 plot)
XLabel=Missing Momentum
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Nm1_deltaY
Title=di-jet rapidity difference (N-1 plot)
XLabel=$\Delta y(jj)$
YLabel=Entries
LogY=0
Rebin=5
# END PLOT




### Lepton plots
# BEGIN PLOT /MC_VBSVAL/lep1_pt
Title=1st lepton $p_T$
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/lep2_pt
Title=2nd lepton $p_T$
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/lep1_eta
Title=1st lepton $\eta$
XLabel=$\eta$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/lep2_eta
Title=2nd lepton $\eta$
XLabel=$\eta$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/mll
Title=Lepton invariant mass
XLabel=m(ll) (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT

### Jet plots
# BEGIN PLOT /MC_VBSVAL/jet1_pt
Title=1st jet $p_T$
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /MC_VBSVAL/jet2_pt
Title=2nd jet $p_T$
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /MC_VBSVAL/jet3_pt
Title=3rd jet $p_T$ (after 2 fiducial jets selected)
XLabel=$p_T$ (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /MC_VBSVAL/jet1_eta
Title=1st jet $\eta$
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
Rebin=6
# END PLOT
# BEGIN PLOT /MC_VBSVAL/jet2_eta
Title=2nd jet $\eta$
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
Rebin=6
# END PLOT
# BEGIN PLOT /MC_VBSVAL/jet3_eta
Title=3rd jet $\eta$ (after 2 fiducial jets selected)
XLabel=$\eta$ (GeV)
YLabel=Entries
LogY=0
Rebin=6
# END PLOT
# BEGIN PLOT /MC_VBSVAL/oppHem
Title=$\eta(j1)*\eta(j2)$
XLabel=$\eta(j1)*\eta(j2)$
YLabel=Entries
LogY=0
# END PLOT

# BEGIN PLOT /MC_VBSVAL/mjj
Title=di-jet invariant mass
XLabel=m(jj) (GeV)
YLabel=Entries
LogY=0
Rebin=5
# END PLOT
# BEGIN PLOT /MC_VBSVAL/mjj_low
Title=di-jet invariant mass (zoomed)
XLabel=m(jj) (GeV)
YLabel=Entries
LogY=0
Rebin=2
# END PLOT

### Final selection plots
# BEGIN PLOT /MC_VBSVAL/deltaPhi_jj
Title=di-jet azimuthal angle difference
XLabel=$\Delta\phi(jj)$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/deltaEta_jj
Title=di-jet $\eta$ difference
XLabel=$\Delta\eta(jj)$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/deltaR_jj
Title=di-jet $\Delta$R
XLabel=$\Delta$R($jj$)
YLabel=Entries
LogY=0
Rebin=2
# END PLOT
# BEGIN PLOT /MC_VBSVAL/deltaPt_jj
Title=di-jet $\Delta~P_{T}$
XLabel=$\Delta~P_{T}(jj)$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/etaStar3
Title=$\eta(j3) - (\eta(j1)+\eta(j2)/2$
XLabel=$\eta(j3) - (\eta(j1)+\eta(j2)/2$
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/lep_centrality
Title=Lepton centrality
XLabel=Centrality
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/NJets
Title=Jet Multiplicity
XLabel=Number of Jets
YLabel=Entries
LogY=0
# END PLOT
# BEGIN PLOT /MC_VBSVAL/Wmass
Title=W candidate mass (works on e$\mu$ only)
XLabel=W mass (GeV)
YLabel=Entries
LogY=1
# END PLOT
# BEGIN PLOT /MC_VBSVAL/missingET
Title=Missing Momentum
XLabel=Missing Momentum
YLabel=Entries
LogY=0
# END PLOT



